import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


class BecomePartners extends StatefulWidget {
  @override
  _BecomePartnersState createState() => _BecomePartnersState();
}

class _BecomePartnersState extends State<BecomePartners> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
      url: "https://www.bahfed.com/about/become-partner",
      withZoom: true,
      hidden: false,
    );
  }
}
