import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


class NasaSewpV extends StatefulWidget {
  @override
  _NasaSewpVState createState() => _NasaSewpVState();
}

class _NasaSewpVState extends State<NasaSewpV> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
      url: "https://www.bahfed.com/sewp",
      withZoom: true,
      hidden: false,
    );
  }
}
