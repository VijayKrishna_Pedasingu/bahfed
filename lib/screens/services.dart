import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
    url: "https://www.bahfed.com/services",
      withZoom: true,
      hidden: false,
    );
  }
}
