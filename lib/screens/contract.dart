import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class Contract extends StatefulWidget {
  @override
  _ContractState createState() => _ContractState();
}

class _ContractState extends State<Contract> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
      url: "https://www.bahfed.com/services/contract-vehicles",
      withZoom: true,
      hidden: false,
    );
  }
}
