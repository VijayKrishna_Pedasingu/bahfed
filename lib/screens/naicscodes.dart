import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


class NaicsCodes extends StatefulWidget {
  @override
  _NaicsCodesState createState() => _NaicsCodesState();
}

class _NaicsCodesState extends State<NaicsCodes> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
      url: "https://www.bahfed.com/services/naics-codes",
      withZoom: true,
      hidden: false,
    );
  }
}
