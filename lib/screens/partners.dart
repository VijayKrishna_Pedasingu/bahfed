import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


class Partners extends StatefulWidget {
  @override
  _PartnersState createState() => _PartnersState();
}

class _PartnersState extends State<Partners> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
      url: "https://www.bahfed.com/about/vendor-relationships",
      withZoom: true,
      hidden: false,
    );
  }
}
