import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ItServices extends StatefulWidget {
  @override
  _ItServicesState createState() => _ItServicesState();
}

class _ItServicesState extends State<ItServices> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('BAHFED CORP'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
//      url: "https://www.bahfed.com",
      url: "https://www.bahfed.com/services/professional-it-services",
      withZoom: true,
      hidden: false,
    );
  }
}
