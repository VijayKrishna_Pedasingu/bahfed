import 'package:bahfed/screens/about.dart';
import 'package:bahfed/screens/become_partner.dart';
import 'package:bahfed/screens/careers.dart';
import 'package:bahfed/screens/certifications.dart';
import 'package:bahfed/screens/contact.dart';
import 'package:bahfed/screens/contract.dart';
import 'package:bahfed/screens/itServices.dart';
import 'package:bahfed/screens/naicscodes.dart';
import 'package:bahfed/screens/nasa%20sewp%20v.dart';
import 'package:bahfed/screens/news.dart';
import 'package:bahfed/screens/partners.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:carousel_pro/carousel_pro.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  theme: ThemeData(
//        primaryColor: Colors.orangeAccent,
    primarySwatch: Colors.orange,
  ),
  home: Splashscreen(),
));

class Splashscreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 5,
      navigateAfterSeconds: Homepage(),
      image: Image.asset('assets/bahfedlogo.png'),
      photoSize: 100.0,
      backgroundColor: Colors.white,
      loaderColor: Colors.white,
    );
  }
}

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {

  @override
  Widget build(BuildContext context) {

    const List<String> abc = ['assets/grid/g1.jpg','assets/grid/g2.jpg','assets/grid/g3.jpg','assets/grid/g4.jpg'];

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 5),
              child: Text(
                'BAHFED CORP',
              ),
            )
          ],
        ),

        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.phone,),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Contact()),
              );
            },
          )
        ],

      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
//              child: Text('Drawer Header'),
              child: Center(
                child: ListTile(
                  leading: CircleAvatar(
                    child: Image.asset('assets/slider/logo2.png'),
                    maxRadius: 20,
                  ),
                  title: Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Text('BAHFED CORP'),
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.orange,
              ),
            ),
            ListTile(
              leading: Icon(Icons.home,),
              title: Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ExpansionTile(
              leading: Icon(Icons.info_outline,),
              title: Text('About'),
              children: <Widget>[
                ListTile(title: GestureDetector(child: Text('Vendor RelationShips'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Partners()),
                  );
                },)),
                ListTile(title: GestureDetector(child: Text('Become a partner'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BecomePartners()),
                  );
                },)),
                ListTile(title: GestureDetector(child: Text('Certifications'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Certifications()),
                  );
                },)),
//                Text('Vendor RelationShips'),
//                Text('Become a partner'),
//                Text('Certifications'),
              ],
            ),
            ExpansionTile(
              leading: Icon(Icons.settings_applications,),
              title: Text('Services'),
              children: <Widget>[
                ListTile(title: GestureDetector(child: Text('Contract Vehicles'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Contract()),
                  );
                },)),
                ListTile(title: GestureDetector(child: Text('NASA SEWP V'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NasaSewpV()),
                  );
                },)),
                ListTile(title: GestureDetector(child: Text('Professional IT Services'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ItServices()),
                  );
                },)),
                ListTile(title: GestureDetector(child: Text('NAICS Codes'),onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NaicsCodes()),
                  );
                },)),
//                Text('Vendor RelationShips'),
//                Text('Become a partner'),
//                Text('Certifications'),
              ],
            ),
            /*ListTile(
              leading: Icon(Icons.info_outline,),
              title: Text('About'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => About()),
                );
//                Navigator.pop(context);
              },
            ),*/
            /* ListTile(
              leading: Icon(Icons.settings_applications,),
              title: Text('Services'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Services()),
                );
//                Navigator.pop(context);
              },
            ),*/
            ListTile(
              leading: Icon(Icons.library_books,),
              title: Text('Careers'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Careers()),
                );
//                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.airplay,),
              title: Text('News'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => News()),
                );
//                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.phone,),
              title: Text('Contact'),
              onTap: () {

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Contact()),
                );
//                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
              child: SizedBox(
//

                height: 200.0,
                width: 400.0,
                child: Carousel(
                  boxFit: BoxFit.cover,
                  dotSize: 4.0,
                  dotSpacing: 15.0,
                  indicatorBgPadding: 2.0,
                  dotBgColor: Colors.transparent.withOpacity(0.1),
                  autoplay: true,
                  borderRadius: true,
                  images: [
                    Image.asset('assets/slider/1.png',fit: BoxFit.cover,),
                    Image.asset('assets/slider/2.jpg',fit: BoxFit.cover),
                    Image.asset('assets/slider/3.jpg',fit: BoxFit.cover),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.all(9),
              child: Text(
                'Welcome to the BahFed Corp',
                style: TextStyle(
                  fontSize: 22,
                  decoration: TextDecoration.underline,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 5,),
            Padding(
              padding: EdgeInsets.all(9),
              child: Text(
                'Incorporated in 2011 in Portland, Oregon, BahFed Corp specializes in the timely and accurate delivery of IT products, commodities, and support solutions. As a small, veteran- and minority-owned business operating in a historically underutilized business zone (HUBZone). ',
                maxLines: 5,
                style: TextStyle(
                  fontSize: 14,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
            SingleChildScrollView(
              scrollDirection:Axis.horizontal,
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    child: Card(
                      child: Column(children: <Widget>[
                        Center(
                          child: Container(
                            height: 210,

                            child: Image.asset('assets/grid/g1.jpg',fit: BoxFit.cover,),

                          ),
                        ),

                        Text('News and Events')

                      ],),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => News()),
                      );
                    },
                  ),
                  GestureDetector(
                    child: Card(
                      child: Column(children: <Widget>[
                        Center(
                          child: Container(
                            height: 210,

                            child: Image.asset('assets/grid/g2.jpg',fit: BoxFit.cover,),

                          ),
                        ),

                        Text("We're Certified!")

                      ],),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Certifications()),
                      );
                    },
                  ),
                  GestureDetector(
                    child: Card(
                      child: Column(children: <Widget>[
                        Center(
                          child: Container(
                            height: 210,

                            child: Image.asset('assets/grid/g3.jpg',fit: BoxFit.cover,),

                          ),
                        ),

                        Text('Professional IT Services')

                      ],),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ItServices()),
                      );
                    },
                  ),
                  GestureDetector(
                    child: Card(
                      elevation: 10,
                      child: Column(children: <Widget>[
                        Center(
                          child: Container(
                            height: 210,
                            child: Image.asset('assets/grid/g4.jpg',fit: BoxFit.cover,),

                          ),
                        ),

                        Text('Meet Our Partners',)

                      ],),
                    ),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Partners()),
                      );
                    },
                  )

                ],
              ),
            ),
            SizedBox(height: 10,),

            Container(
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              child: SingleChildScrollView(
                scrollDirection:Axis.horizontal,                child: Row(
                children: <Widget>[
                  OutlineButton(
                      color: Colors.white,
                      child: Text('About',
                        style: TextStyle(color: Colors.white),),
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => About()),
                        );
                      }),
                  OutlineButton(
                      color: Colors.white,
                      child: Text('Certifications',
                        style: TextStyle(color: Colors.white),),
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Certifications()),
                        );
                      }),
                  OutlineButton(
                      color: Colors.white,
                      child: Text('Contracts',
                        style: TextStyle(color: Colors.white),),
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Contract()),
                        );
                      }),
                  OutlineButton(
                      color: Colors.white,
                      child: Text('Contact',
                        style: TextStyle(color: Colors.white),),
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Contact()),
                        );
                      }),
                ],
              ),
              ),
            ),

          ],


        ),
      ),
    );
  }
}
